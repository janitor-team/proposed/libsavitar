Source: libsavitar
Priority: optional
Maintainer: Debian 3-D Printing Packages <3dprinter-general@lists.alioth.debian.org>
Uploaders: Gregor Riepl <onitake@gmail.com>,
 Christoph Berg <myon@debian.org>,
Build-Depends: debhelper-compat (= 12),
 cmake (>= 3.12), dh-python,
 libpugixml-dev (>= 1.7),
 python3-all-dev,
 python3-sip-dev (>> 4.19.12+dfsg-1) | python3-sip-dev (<< 4.19.11+dfsg-1)
Standards-Version: 4.5.0
Section: libs
Homepage: https://github.com/Ultimaker/libSavitar
Vcs-Browser: https://salsa.debian.org/3dprinting-team/libsavitar
Vcs-Git: https://salsa.debian.org/3dprinting-team/libsavitar.git

Package: libsavitar0
Architecture: any
Multi-Arch: same
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: 3MF file handling library (shared library)
 Savitar is a C++ library with Python 3 bindings for
 reading and writing 3MF files.
 .
 3MF is an interchange format for sharing 3D models and other 3D printing
 data between related software and 3D printers.
 It is XML based and standardised by the 3MF consortium.
 .
 This package contains the shared library.

Package: libsavitar-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: libsavitar0 (= ${binary:Version}), libpugixml-dev (>=1.7), ${misc:Depends}
Description: 3MF file handling library (development files)
 Savitar is a C++ library with Python 3 bindings for
 reading and writing 3MF files.
 .
 3MF is an interchange format for sharing 3D models and other 3D printing
 data between related software and 3D printers.
 It is XML based and standardised by the 3MF consortium.
 .
 This package contains C++ headers and other development files.

Package: python3-savitar
Section: python
Architecture: any
Multi-Arch: same
Depends: libsavitar0 (= ${binary:Version}), ${shlibs:Depends}, ${misc:Depends},
 ${python3:Depends}, ${sip3:Depends}
Description: 3MF file handling library (Python bindings)
 Savitar is a C++ library with Python 3 bindings for
 reading and writing 3MF files.
 .
 3MF is an interchange format for sharing 3D models and other 3D printing
 data between related software and 3D printers.
 It is XML based and standardised by the 3MF consortium.
 .
 This package contains the Python 3 bindings.
